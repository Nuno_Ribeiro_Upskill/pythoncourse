# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 02:50:45 2020

@author: Nuno
"""

# Exercise 1  
"- Provide operations with circles, namely, calculation of the perimeter and area given the radius. Add the sphere volume function code in your module"
"- Provide operations with squares, namely, calculation of the perimeter and area given the length of the side."
" - Provide the possibility to ask the user for a value and writes on the screen the perimeter and the area of the circle and square based in the radius and side measurement."
import math
import string

#defining the circles function
def circles(radius):
	#calculating the area
	area= math.pi*(radius*radius)
	#calculating the perimeter
	perimeter=2*math.pi
	#print the result
	print("Circle: Perimeter = "+str(perimeter)+"   Area = "+str(area))

#defin the squares function
def squares(length):
	#calculating the area
	area= length*length
	#calculating the perimeter
	perimeter=4*length
	#print the result
	print("Square : Perimeter = "+str(perimeter)+"   Area = "+str(area))

#main function
def main():
	#getting the input value
	val=float(input("Give the value for radius and side length : "))
	#calling two functions
	circles(val)
	squares(val)
#calling the main
main()

"Write a function that takes as a parameter a list of integers."
"The function must return a tuple with two integer values f1 and f2, where f1 is the list element with lowest frequency (lowest number of occurrences in the list) and f2 is the element with the highest frequency." 
"Tip: use a dictionary to compute the frequencies of the list elements."
"The following function must be implemented: def frequencies (v) Input = [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9, 7] Output = ([f1], [f2]) f1 = [5,7] f2=[1] def frequencies (v)"

# Python program to count the frequency of  
# elements in a list using a dictionary 
  
def Frequency(my_list): 
  
    # Creating an empty dictionary  
    max1=1
    min1=100
    freq = {} 
    f1=[]
    f2=[]
    for item in my_list: 
        if (item in freq): 
            freq[item] += 1
        else: 
            freq[item] = 1
  
    for key, value in freq.items(): 
    	if(min1>value):
    		min1=value
    	if(max1<value):
    		max1=value

    for key, value in freq.items():
    	if(value==min1):
    		f1.append(key)
    	if(value==max1):
    		f2.append(key)
    print(f1)
    print(f2)
  
# Driver function 
if __name__ == "__main__":  
    my_list = [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9, 7]
  
    Frequency(my_list) 