# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
################################################################

# Aula 3  

## Exercise 1 - Write a code that reads two numbers and then a character that represents a arithmetic operator (‘+’, ‘-’, ‘*’ or ‘/’). Your program should then print the result operator applied to the two numbers given.

## 1.a.
# Python3 program to add two numbers 
num1 = 15
num2 = 12
  
# Adding two numbers 
sum = num1 + num2 
  
# Printing values 
print("Sum of {0} and {1} is {2}" .format(num1, num2, sum))

# Store input numbers
num1 = input('Enter first number: ')
num2 = input('Enter second number: ')

# Add two numbers
sum = float(num1) + float(num2)

# Display the sum
print('The sum of {0} and {1} is {2}'.format(num1, num2, sum))

## Exercise 2 - Consider the following menu:
## 2.a.

""" 1 - Pão Alentejano
2 2 - Bolo Lêvedo [dos Açores]
3 3 - Bolo do Caco [da Ilha da Madeira]
4 4 - Broa
5 5 – I want to leave """

'''Your program should:'''
'''•	print the menu; 
•	read a number from 1 to 5;
•	and print the option menu corresponding to the number read. 
This must be repeated until the user selects the option 5.'''

ans=True
while ans:
    print("""
    1.Pão Alentejano.
    2.Bolo Lêvedo [dos Açores].
    3.Bolo do Caco [da Ilha da Madeira]
    4.Broa
    5.I want to leave
    """)
    ans=int(input("What would you like to do? "))
    if ans== 1:
      print("Pão Alentejano.")
      break
    elif ans== 2:
      print("Bolo Lêvedo [dos Açores].")
      break
    elif ans== 3:
      print("Bolo do Caco [da Ilha da Madeira]")
      break
    elif ans== 4:
      print("Broa")
      break
    elif ans== 5:
      print("\n Goodbye") 
      break
    else: 
        print ("Unknown Option Selected!")


## Exercise 3 -  What will be printed by the program below? Assume that the value of D in the ord(<letter>) of the first letter of your name. Example: ord(A) = 65
#use ord  funtion to get the unicode value of the letter
#My name is Nuno. The first letter is N
#ord(N) = 78 = D #ASCII value

## 3.a.
ch = input("Enter your name : ")

D=ord(ch[0])
x = 5 + D
y = 0
while True :
	y = (x % 2) + 10 *y
	x = x // 2
	print ('x =', x, 'y =', y)
	if x == 0:
		break

while y != 0:
	x = y % 100
	y = y // 10
	print ('x =', x, 'y =', y)



## Alternative solution. Input for my name first letter

first_letter=input("My Name First letter is ")

D=ord(first_letter)


x = 5 + D
y = 0
while True :
	y = (x % 2) + 10 *y
	x = x // 2
	print ('x =', x, 'y =', y)
	if x == 0:
		break

while y != 0:
	x = y % 100
	y = y // 10
	print ('x =', x, 'y =', y)

## Exercise 4 – Write a program that reads a positive integer n and prints n lines with the following format (example for n = 5 => 5 spaces) 

## 4.a.
n=int(input("Enter n: "))
for i in range(n+1):
    if i==1:
        print (i)
    if i > 1:
        z=i-2
        print (" "*z, i)