# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 14:25:36 2020

@author: Nuno Ribeiro
"""
################################################################

# Aula 4 Exercises
##Exercise 1 - What do the following expressions evaluate to?

## 1.a.
#### False:
(5 > 4) and (3 == 5)
#### False:
not (5 > 4)
#### True:
(5 > 4) or (3 == 5)
#### False:
not ((5 > 4) or (3 == 5))
#### False:
(True and True) and (True == False)
#### True:
(not False) or (not True)

##Exercise 2 - Identify the three blocks in this code:

## 2.a.
spam = 0
if spam == 10:
### indent increased, block A:
      print('eggs') 
### still block A:
      if spam > 5:
### still block A, indent increased, block B inside block A:
           print('bacon')
### still block A, indent decreased, block B ended in line above:
      else:
### still block A, indent increased, block C inside block A: 
          print('ham')
### # still block A, indent decreased, block C ended in line above:
      print('spam')
### indent decreased, block A ended in line above:   
print('spam')

### The three blocks are everthing inside the if statement and the lines print ('bacon') and print ('ham')

##Exercise 3 - Write a code that reads n numbers and print how many of them are in the following ranges: [0; 25], [26; 50], [51; 75] and [76; 100]. For example, for n = 10 and the following numbers {2; 61; -1; 0; 88; 55; 3; 121; 25; 75} , your program should print:
'''1 [0 ,25]: 4
2 [26 ,50]: 0
3 [51 ,75]: 3
4 [76 ,100]: 1'''

## 3.a.
n = int(input("How many numbers:"))
print("Enter the numbers one by one")

range1_count=0
range2_count=0
range3_count=0
range4_count=0

for i in range(n):
	number=int(input())
	if(number>=0 and number<=25):
		range1_count=range1_count+1
	if(number>=26 and number<=50):
		range2_count=range2_count+1
	if(number>=51 and number<=75):
		range3_count=range3_count+1
	if(number>=76 and number<=100):
		range4_count=range4_count+1

print("1 [0,25]: "+str(range1_count))
print("2 [26,50]: "+str(range2_count))
print("3 [51,75]: "+str(range3_count))
print("4 [76,100]: "+str(range4_count))

##Exercise 4 - Use 10 strings methods of your choose to get information from this piece of text 
'''Catching up
Start with The Portuguese: The Land and Its People (3) by Marion Kaplan (Penguin), a one-volume introduction ranging from geography and history to wine and poetry, and Portugal: A Companion History (4) by José H Saraiva (Carcanet Press), a bestselling writer and popular broadcaster in his own country.'''

## 4.a.
txt = '''Catching up
Start with The Portuguese: The Land and Its People (3) by Marion Kaplan (Penguin), a one-volume introduction ranging from geography and history to wine and poetry, and Portugal: A Companion History (4) by José H Saraiva (Carcanet Press), a bestselling writer and popular broadcaster in his own country.'''
x = txt.title()
print(x)
x = txt.upper()
print(x)
x = txt.casefold()
print(x)
x = txt.count("by")
print(x)
x = txt.endswith(".")
print(x)
x = txt.find("Start")
print(x)
x = txt.isalpha()
print(x)
x = txt.islower()
print(x)
x = txt.isupper()
print(x)
x = txt.rfind("by")
print(x)

##Two More string Methods
x = txt.rsplit(",")
print(x)
x = txt.startswith("Catching")
print(x)

##Exercise 5 - Write a code that reads a number (0-16) in the decimal base and prints that number in the binary base.

## 5.a.
#Getting input for the decimal number
num=int(input("Enter the decimal number:"))

#Defing DecimalToBinary function
def DecimalToBinary(num):
	#if the num>1 call the Dec_to_binary funtion with the input of num//2
	if num>1:
		DecimalToBinary(num//2)
	#the base case for recursive funtion when num=0,num=1 print num%2
	print(num % 2, end = '')

#print this for the output
print("The Binary representation of is: ",end='')

#Calling the function
DecimalToBinary(int(num))
#print for new line
print('')