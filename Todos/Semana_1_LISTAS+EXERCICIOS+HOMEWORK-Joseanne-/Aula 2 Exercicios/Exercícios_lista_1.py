# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
################################################################

# Aula 2 Exercises
## Exercise 1 - Answer what type of object (variable) should be used to store each of the following information:
'''a.	A person's age.
b. 	The area of your yard in square meters.
c. 	The amount of money in your bank account 
d.	The name of your favorite songs.'''

## 1.a. A person's age
age = int(input("Enter your age: "))
print(age)
print(type(age))

## 1.b. The area of your yard in square meters.
area_square = float(input("The area of your yard in square meters "))
print(area_square)
print(type(area_square))

## 1.c. The amount of money in your bank account 
bank_account = float(input("The amount of money in your bank account "))
print(bank_account)
print(type(bank_account))

## 1.d. The name of your favorite songs
favorite_songs = input("The name of your favorite songs ")
print(favorite_songs)
print(type(favorite_songs))

## Exercise 2 - Write a Python program that accepts a word from the user and reverse it

## 2.a. 
word = input("Input a word to reverse: ")
 
for char in range(len(word) - 1, -1, -1):
  print(word[char], end="")
print("\n")
s = "abc"
print (s)


## Exercise 3 - Initialize the string "abc" on a variable named "s":
# Exercise with strings
# Python code to demonstrate string length 
# Using len 

## 3.a. Use a function to get the length of the string.
s='abc'
print(len(s))

## 3.b Write the necessary sequence of operations to transform the string "abc" in "aaabbbccc". Suggestion: Use string concatenation and string indexes.'''
print('{0}{0}{0}{1}{1}{1}{2}{2}{2}'.format('a','b','c'))
print(s[0]*3+s[1]*3+s[2]*3)

## Exercise 4 - Initialize the string "aaabbbccc" on a variable named "s":

## 4.a. Use a function that allows you to find the first occurence of "b" in the string, and the first occurence of "ccc".
s='aaabbbccc'
print(s.find('b'))
print(s.find('ccc'))

## 4.b. Use a function that allows you to replace all occurences of "a" to "X", and then use the same function to change only the first occurence of "a" to "X".'''
print(s.find('a'))
print(s.replace(s[4],'X'))
print(s.replace('a','X',1))

## Exercise 5 - Starting from the string "aaa bbb ccc", what sequences of operations do you need to arrive at the following strings? You can use the "replace" function.

## 5.a. "AAA BBB CCC"
s='aaa bbb ccc'
s=s.upper()
print(s)
## 5.a. "AAA bbb CCC"'''
print(s.replace('B','b'))


## Exercise 6 - Consider the code snippet below:
'''After executing this code snippet, what will be the value stored in each variable?'''

## 6.a
a=10
#### # Value of the variable is 10:
b=a
#### # Value of the variable is 9:
c=9
#### # Value of the variable is 9:
d=c
#### # Final Value of the variable is 10:
c=c+1
 


## Exercise 7 - Write a code that reads two integer values in the variables x and y and change the content of the variables. For example, assuming that x = 2 and y = 10 were the values read, your program should make x = 10 and y = 2. Redo this problem using only x and y as variables.
# Python program to swap two variables

## 7.a
x = 2
y = 10

# To take inputs from the user
x = input('Enter value of x: ')
y = input('Enter value of y: ')

# Create a temporary variable and swap the values
temp = x
x = y
y = temp

print('The value of x after swapping: {}'.format(x))
print('The value of y after swapping: {}'.format(y))

## Exercise 8 - Consider a program that should classify a number as odd or even and, in addition, classify it as less than 100 or greater than or equal to 100. The solution below does this classification correctly?

## 8.a
print (" Enter a number  :")
a = int ( input ())
if a % 2 == 0 and a < 100:
       print ("the number is even and smaller than 100")
else :
       if a >= 100:
                 print ("The number is even and equal or higher than 100")
if a % 2 != 0 and a < 100:
              print ("The number is odd and smaller than 100")
else :
        if a >= 100:
           print ("The number is odd and equal or higher than 100")

###The classification is not correct. It misses the condition to fulfill the requirement when the input number is is higher than 100.


### I suggest this program that should classify a number as odd or even and, in addition, classify it as less than 100 or greater than or equal to 100:
print (" Enter a number  :")
a = int ( input ())
if a > 100:
    if a % 2 == 0 : 
        print ("the number is even and higher than 100") 
    else:
        print ("The number is odd  higher than 100")
if a == 100:
    if a % 2 == 0 :
        print ("the number is even and equal 100")
    else:
        print ("The number is odd and equal 100") 
if a < 100: 
    if a % 2 == 0 :
        print ("the number is even and smaller than 100")
    else:
        print ("The number is odd and smaller than 100")