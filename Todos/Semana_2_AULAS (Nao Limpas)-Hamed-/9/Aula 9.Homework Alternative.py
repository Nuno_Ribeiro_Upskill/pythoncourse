# -*- coding: utf-8 -*-
"""
Created on Sat Oct 31 23:01:16 2020

@author: Nuno
"""

my_obj = {1: "one", 2: "two"}
for i, j in my_obj.items:
 	print("i is: ", i)
 	print("j is: ", j)