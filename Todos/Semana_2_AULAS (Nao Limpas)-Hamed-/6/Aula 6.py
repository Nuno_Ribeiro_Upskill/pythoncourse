# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 05:46:11 2020

@author: Nuno Ribeiro
"""


a2 = 2
b = [1, 2 ,3]
c= [[1, 2]], [[3, 4]]
c = [[[1, 4], 2], [[3, 4]]]

_ = 45
_
print(_)
name_2435 = 32
print(name_2435)

from keyword import iskeyword
print(iskeyword("if"))
a = 12

b = 15

# t = a

# a = b

# b = t
a,b =b, a
# a = b

# b
# Out[13]: 15

# a == b
# Out[14]: True
type(True)
type(2*3)

# a = int(a)
# 2**3
# 2**

# a = 34
# b= 21
# a = a + b
# a += b

c = bin(a)

type(c)
c =bytes(a)
d=bytearray(a)
a = 13
# a « 1
# a » 2
# import math
# range(2, 10, 1)
# list(range(2, 10, 1))
# list(range(2, 10, -1))
# # list(range(22, 10, -1))
# list(range(2, 12 ))
# list(range(12 ))

# # for j in range(5, 10, 2):
#     print(j, end= ""

end " com espaco

name = "farshid"
v = "aeiou"
for ch in name:
    if ch in name:
        if ch in v:
            print(ch)


name = "farshid joe sara"
v = "aeiou"
for ch in name:
     if ch in v:
         print(ch)

m =[]            
name = "farshid joe sara "
v = "aeiou"
for ch in name:
    if ch in v:
        m.append(ch)
            
print(set(m))

a;

