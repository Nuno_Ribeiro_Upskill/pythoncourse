# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
# Aula 9 PPT Tupple Pages 45-50

#Homework 1

#Input =[(1,2,3) , (4,5,6)]
#Output =[(1,2,9) , (4,5,9)]
#Try last example again with another way

b=[]
a = [(1,2,3) , (4,5,6)]

for i in range(len(a)):
	temp=[]
	for j in range(len(a[i])-1):
		#append 1st 2 element of every row
		temp.append(a[i][j])
	#append 9 to the 2nd element of every t=row
	temp.append(9)
	#append whole updatede row to the b
	b.append(temp)
print(b)

#Homework 2
'''By using unpacking method define my_obj
to avoid raising error?
my_obj = …
for i, j in my_obj :
print(“i is: ”, i)
print(“j is: ”, j)
When can we use “for i, j in my_obj”?
For example by defining my_obj = (1, 3)
We get this Error:'''

#I add {} and inside that I can define my_obj={(1,3)} because we use {} to define an array
my_obj = {(1,3)}
for i, j in my_obj :
	print("i is: ", i)
	print("j is: ", j)
    
#Alternative solution   
my_obj = [(1, 2), (3, 4) , (5, 6) ]
for i, j in my_obj :
 	print("i is: ", i)
 	print("j is: ", j)
  
#Alternative solution  
my_obj = ([1, 2], [3, 4], [5, 6])
for i, j in my_obj :
 	print("i is: ", i)
 	print("j is: ", j)

#Alternative solution      
my_obj = ([1, 2, 11 ], [3, 4, 22] , (5, 6, 33))
for i, j, k in my_obj :
 	print("i is: ", i)
 	print("j is: ", j)
 	print("k is: ", k)
     
#Alternative solution
my_obj = ([1, 2, 11 ], [3, 4, 22] , (5, 6, 33))
for i, j, k in my_obj :
 	print(i, j, k)

#Homework 3
# Input [(1, 3), (2, 4), ("A", 8)]
# Output [(1, 2, ‘A’), (3, 4, 8)]
# Do Not use zip () function

#define the input
list_1 = [(1, 3),(2, 4),("A", 8)]
#initialize list for output
output=[]
#for loop to go tho the every column of the input1
for i in range(len(list_1[0])):
	#initialize tem array
	temp=[]
	#for loop to gor through every row
	for j in range(len(list_1)):
		#append j the elemnt of every row to tmp 
		temp.append(list_1[j][i])
	#append every temp in to output
	output.append(temp)

print(output)





