# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
################################################################

# Aula 8 PPT List Page 58

#Homework 1
m = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

#define funtion for priting the first row
def printFisrtRow():
	#getting the first row to array called temp
	temp=m[0]
	#use for loop to get the every element in the temp array
	for key in temp:
		#print them one by one
		print(key,end=" ")
	#use for printing new line
	print("")

#define function for printing first column 
def printFirstColumn():
	#for loop to going through every row
	for temp in m:
		#print 1st element of each row
		print(temp[0],end=" ")
	print("")

#define funtion for peinting main diameter
def printMainDiameter():
	#for loop loop to go through the diameter 
	for i in range(len(m)):
		#taking the i th row
		temp=m[i]
		#printing the i th element of i th row 
		print(temp[i],end=" ")
	print("")

def printAnotherDiameter():
	#for loop loop to go through the diameter 
	for i in range(len(m)):
		#taking the i th row
		temp=m[i]
		#printing the i th element of last index-i th row 
		print(temp[len(m)-i-1],end=" ")
	print("")

#define funtion to calculate the sum of rows
def calculateSumRow():
	for temp in m:
		print(sum(temp),end=" ")
	print("")

#definr function to calulate the sum of the column
def calculateSumColumn():
	#get the length of array
	length=len(m)
	#for loop to go thorou e=evry element
	for i in range(length):
		#intialize valu for sum
		temp_sum=0
		for j in range(length):
			#get the j th row as temp
			temp=m[j]
			#add the value of j the columb  of the i th row
			temp_sum=temp_sum +temp[i]
		print(temp_sum,end=" ")
	print("")

#call the function foe printing the first row
printFisrtRow()
#Call the funtion for printing the first column
printFirstColumn()
#call the funtion to print the main diameter
printMainDiameter()
#call the funtion to print another diameter
printAnotherDiameter()
#call the fution to print the sum of rows
calculateSumRow()
#call the funtion to print the sum fof column
calculateSumColumn()

