# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
# Aula 11 PPT Set Pages 32-36

# Homework 1
# Find match key: value in 2 dictionaries 
'''d1 = {'a': 1, 'b': 3, 'c': 2}
d2 = {'a': 2, 'b': 3, 'c': 1}'''

d1 = {'a': 1, 'b': 3, 'c': 2}
d2 = {'a': 2, 'b': 3, 'c': 1}

for (key, value) in set(d1.items()) & set(d2.items()):
    print('%s: %s is present in both d1 and d2' % (key, value))


# Homework 2
# Find the intersection of 2 intervals 
#getting input for the intervals
a=int(input("Enter the value a of interval: "))
b=int(input("Enter the value b of interval: "))
c=int(input("Enter the value c of interval: "))
d=int(input("Enter the value d of interval: "))

#for the non intersection

if(a<=d and a>=c):
	print("["+a+","+d+"]")
elif(b>=c and b<=d):
	print("[",c,",",b,"]")
else:
	print("No intersection")