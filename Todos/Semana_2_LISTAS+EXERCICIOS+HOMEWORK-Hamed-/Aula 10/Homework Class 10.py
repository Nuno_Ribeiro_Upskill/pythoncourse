# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 16:11:30 2020

@author: admin
"""
# Aula 10 PPT dictionary v1 Page 51

#Homework 1
#Create a "person" dictionary with this details


#person dictionary
person={
	#name atribute
	"name":"Nuno",
	#phone atribute wich include home and mobile
	"phone":{
	"home":"01-4455",
	"mobile":"918-123456"
	},
	#children dictionry with the names of children
	"children":{
	"0":"Olvia",
	"1":"Sophia"
	},
	#
	"age":48
}

print(len(person))
print(person['phone']['home'])
print(person['phone']['mobile'])
print(person['children'])
print(person['children']['0'])
print(person.pop('age'))



