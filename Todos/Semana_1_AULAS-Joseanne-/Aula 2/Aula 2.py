# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
################################################################

# Aula 2 PPT 1- IntroductionToPython 

################################################
frase = "Lisbon is in Portugal"
print(frase)
print(frase[18])
print(frase[14])
print(frase[0:6])
city_name = frase[0:6]
print(city_name)
country_name = frase[13:21]
print(country_name)

################################################

##Exercise 6 -

## 6.a.
#Splitting Strings 

#letters = "abcdefghijklmnopqrstuvwxyz"

#msg =  012345678901234
msg = "Lisbon is in Portugal" #23 lettters
#i strings starts in 0
# i:
print(msg[1])
#Lisbon:
print(msg[0:6])
#bo:
print(msg[3:5])
#Lisbon is:
print(msg[0:9])
#Lisbon is:
print(msg[:9])
#in P:
print(msg[10:14])
# in Portugal:
print(msg[10:]) 
 #Lisbon:
print(msg[:6])
# is in Portugal:
print(msg[6:])
#Lisbon is in Portugal:
print(msg[:6] + msg[6:]) 
#Lisbon is in Portugal:
print(msg[:]) 


##Exercise 7 -

## 7.a.
#Splitting Strings 

# 2 1               0
#       -1098765432119876543210
# msg =  "Lisbon is in Portugal"

#msg =  012345678901234
#letters = "abcdefghijklmnopqrstuvwxyz"
          
msg = "Lisbon is in Portugal" #23 lettters

#Lisbon:
print(msg[-21:-15])  
#Por:
print(msg[-8:-5]) 
#is:
print(msg[-14:-12])
#Portugal:
print(msg[-8:]) 
#in:
print(msg[-11:-8]) 
#Lisbon is in Portugal:
print(msg[-23:]) 
#tuga:
print(msg[-5:-1])
#is in Portugal:
print(msg[7:])
#Lisbon is in Portugal:
print(msg[:])

##Exercise 8 -

## 8.a.
a = 12
b = 7

# 19:
print(a + b)
# 5:  
print(a - b)
# 84: 
print(a * b)
# 1.714285714285714:
print(a / b)
# 1:
print(a // b)
# 5: 
print(a % b)
# <class 'float'>:
print(type(a/b))
# <class 'int'>:
print(type(a // b))

##Exercise 8.1 -

## 8.1.a.
a = 12
b = 3

# 15:
print(a + b)  
# 9: 
print(a - b)
# 36:  
print(a * b)
# 4.0:  
print(a / b)
# 4 integer division, rounded down towards minus infinity:
print(a // b) 
# 0 modulo: the remainder after integer division: 
print(a % b)  

##Exercise 9 - Write a code that take two numbers from user, print the sum 

## 9.a.
num1 = int(input(" Enter first number :"))
num2 =int(input(" Enter second number :"))
sum = num1 + num2
print(str(num1) + " + " + str(num2) + " = " , sum)

num2 =int(input(" Enter second number :"))
print (num2)

##Exercise 10 - Write a code that get 5555
'''Input1: “xxxx----xxxx----5555---" 
Expected output: 5555'''

## 10.a.
credit_str = "xxxx----xxxx----5555---"
new_credit_str = credit_str[credit_str.index("5"):credit_str.index("5") + len("5555")]
print(new_credit_str)

#################################################
#Useful information https://docs.python.org/3/library/sys.html

import sys
print("\nInteger value information: ",sys.int_info)
print("\nMaximum size of an integer: ",sys.maxsize) 

print (2**63 - 1)
#################################################
       
# Aula 3 PPT 2- PythonConditionalsLoops_part1 

pi = 3.1415
radius = float(input("Enter  radius: "))
volume = (4/3*pi*(radius**3))
print(f"{volume:.3f} U")

pi = 3.1415
radius = float(input("Enter  radius: "))
volume = (4/3*pi*(radius**3))
print(f"{4/3*pi**(radius**3):.3f} U")

##Exercise - Change the code above

## 1.a.
pi = 3.1415
radius = float(input("Enter  radius: "))
const = 4/3
power = 3
volume = (const*pi*(radius**power))
print(f"{volume:.3f} U")


pi = 3.1415
radius = float(input("Enter  radius: "))
const = 4/3
power = 3
volume = (const*pi*(radius**power))
print(f"{const*pi**(radius**power):.3f} U")

####################################################################
'''Class 2 - 3 Review
Two new commands to work with the remote repository git pull, git status
To show which data type a variable uses, we use type()
Strings are sequences, we could split sequence using and print it using print(str[_:_]) 
A bit on import sys module and how to use methods available in the module
Exercises on arithmetic operations  
Introduction on how to make decisions using python 
While loop(infinite loop, finite loop)
Basics on fluxograms applied to programming 
Basic analysis on how to make functions (prepare piece of code to be a function)
Update code in files located in the remote repository'''
####################################################################