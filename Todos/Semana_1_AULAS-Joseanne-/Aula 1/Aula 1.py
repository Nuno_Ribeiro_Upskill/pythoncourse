# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
################################################################

# Aula 1 PPT 1- IntroductionToPython 

##Exercise 1 - 

## 1.a. Write a code to print out the string  “Welcome to python course”
print("welcome to python")
print ("Welcome to pythonCourse")

## 1.b. Write a code to print out the result of the operations  8+5*13
print(8+5*13)
## 1.c. Write a code to print out <Python is fun> <number of the days of this month >
print("Python is fun", 31)

'''Concatenating strings   
Creating variables to store values
Splitting lines \n 
and using tabs \t'''

print("Hello" + "Nuno Ribeiro")
print("Hello" ' ' "Nuno Ribeiro")
print("Hello" + "Nuno Ribeiro\n You are splitting this line several\t times\n")
print("Hello"",","Nuno Ribeiro")
Greeting = "Oi"
name = "Nuno Ribeiro"
print(name)
print("Hello", name)
print(Greeting + name)
print(Greeting +' '+ name)
print(type(name))
age=1
print (name, age)
###print (name + age) str+int *ERROR
print("hello" * 100)
print(Greeting * 100)

'''Strings Formatting –f-Strings'''

name = "Nuno Ribeiro"
age = 40
print('%s is %d years old' % (name, age))
print('{} is {} years old' .format(name, age))
print(f'{name} is {age} years old')
print("Please, Enter your name: ") 
student_name = input()
print("welcome,", student_name)

##Exercise 2 - 

## 2.a. Write a code to produce the following output  
print("Student1\tNuno")
print("Student2\tRodrigo")

## 2.b. Display float number with 2 decimal places using print()
print('%.2f' % 458.541315)

#Exercise 3 - 

## 3.a. Write a code to ask for a name, store  it in a variable and print it 
print("Please, Enter your name: ") 
student_name = input()
print(student_name)

#Exercise 4 - 

## 4.a. Write a code that receives your name as input
str1, str2, str3 = input("Enter three string").split()
print(str1, str2, str3)

#Exercise 5 -  

'''You have the following data.
totalMoney = 1000
quantity = 3
price = 450
display above data using string.format() method
expected output: 
I have 1000 dollars so I can buy 3 football for 450.00 dollars.'''

## 5.a. 
quantity = 3
totalMoney = 1000
price = 450
statement1 = "I have {1} dollars so I can buy {0} football for {2:.3f} dollars."
print(statement1.format(quantity, totalMoney, price))


####################################################################
'''Class 1 - Review
Function to print data in the console:  -print()
Function to receive data from the user: -input()
It is possible to store values in variables 
We could use + *  to concatenate strings
Most used variables in basic level are types: strings, int and float.   
It is not possible to concatenate strings and numbers.
It is possible to make + - * / // operations in python.
It is possible to print in several ways (one argument, multiple arguments, one line, multiple lines, one space, multiple spaces, one letter, multiple letters, finite sequence of letters).
Create your own online repository is not that complicated.
Basic git commands add, commit –m, push. 
It is possible to use several IDEs to program in python.'''
###################################################################