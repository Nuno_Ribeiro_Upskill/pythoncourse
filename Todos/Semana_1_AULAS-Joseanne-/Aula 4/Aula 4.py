# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
################################################################

# Aula 4 PPT 3- Loops_part2

n=5
while n > 0 :
    print(n)
    n = n -1
print("Boom!!")
print(n)

## Simple for Loop
for i in [5, 4, 3, 2, 1, 0] :
    print(i)
print("Boom!!")

## For Loop with Strings
friends = ["Ana", "Filipe", "João"]
for friend in friends :
    print("Congrats on your new job")
print ("Done")

## in Word 
for i in [5, 4, 3, 2, 1]:
    print(i)

## Range word    #(https://docs.python.org/3/library/stdtypes.html#ranges)
print ("Before)")
for i in range(5) :
    print(i)
print("After")

## Other range word
print ("Before)")
for i in range(10) :
    print(i)
print("After")

## More ranges
print("Before")
for i in range (2, 6):
    print(i)
print("After")

print("Before")
for i in range (15, 0, -5):
    print(i)
print("After")

## Looping examples
print("Before")
for thing in [9, 41, 12, 3, 74, 15] :
    print(thing)
print("After")

msg = "portugal"
a= msg.capitalize ()
print (a)

## Loop to find the largest number
largest_so_far = -1
print("Before", largest_so_far)
for the_num in [9, 41, 12, 3, 74, 15] :
        if the_num > largest_so_far :
            largest_so_far = the_num
        print(largest_so_far, the_num)
print ("After", largest_so_far)

# Loop to count 
loop_interaction = 0
print("Before", loop_interaction )
for thing in [9, 41, 12, 3, 74, 15] :
    loop_interaction = loop_interaction + 1
    print(loop_interaction , thing)
print("After", loop_interaction )

# Loops to sum
sum = 0
print ("Before", sum)
for thing in [9, 41, 12, 3, 74, 15] :
    sum = sum + thing
    print(sum, thing)
print('After', sum)

# Loop to find the average value
count = 0
sum = 0
print("Before", count, sum)
for value in [9, 41, 12, 3, 74, 15] :
    count = count + 1
    sum = sum + value
    print(count, sum, value)
average_value = sum / count
print('After', count, sum, average_value)

# Loop to select values 
print("Before")
for value in [9, 41, 12, 3, 74, 15] :
    if value > 20:
        print ("Large number", value)
print("After")

# Loop to search using boolean 
found = False
print('Before', found)
for value in [9, 41, 12, 3, 74, 15] : 
   if value == 3 :
       found = True
   else:
       found = False
   print(found, value)
print('After', found)

# Loop to find the smallest value
palavra = None
print ("Before")
for value in [9, 41, 12, 3, 74, 15] :
    if palavra is None :
        palavra = value
    elif value < palavra :
        palavra = value
    print(palavra, value)
print ("after", palavra)

# is and is not Operators
smallest = None
print('Before')
for value in [3, 41, 12, 9, 74, 15] :
    if smallest is None : 
        smallest = value
    elif value < smallest : 
        smallest = value
    print(smallest, value)

print('After', smallest)

###############################################################
"""Class 4 - Review
For Loops 
Python reserved words None, is, is not
For loops applications 
More on fluxograms
Algorithms development (Loops with decision structures)
Debugging on spyder
More on documentation 
More on string methods  (isupper(), capitalize(), find())
Type Conversion int()"""
###############################################################