# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
################################################################

# Aula 5 PPT 4- FunctionsAndMore

# Session 2 revision
def volume_sphere(radius_1):
    pi = 3.1415
    const = 4/3
    power = 3
    volume = (const*pi*(radius_1**power))
    print(f"{volume:.3f} U")
    
radius_1= float(input("Please, Enter  radius: "))    
volume_sphere(radius_1)


import math

radius_1= float(input("Please, Enter  radius: "))    
volume_sphere(radius_1)

def volume_sphere(radius_1, power, const):
    const = 4/3
    power = 3
    print(math.pi)
    volume = (const*math.pi*(radius_1**power))
    print(f"{volume:.3f} U")


def volume_sphere(radius_1, power, const):
     volume = (const*math.pi*(radius_1**power))
     print(f"{volume:.3f} U")

radius_1= float(input("Please, Enter  radius: "))    
const=1
power=3
volume_sphere(radius_1, power, 4/3)


def volume_sphere(radius_1, power, const):
     volume = (const*math.pi*(radius_1**power))
     return volume

radius_1 = float(input("Please, Enter  radius: "))    
const=1
power=3
volume = volume_sphere(radius_1, power, 4/3)
print(f"{volume:.3f} U")

#Building our Own Functions

def thing():
    print('Hello')
    print('Fun')
 
thing()
print('Zip')
thing()


def music_lyrics():
    print("We are here")
    print("We are here for all of us")
    

def food(vegetable):
    if vegetable == "tomato":
        print("I bought tomato")
    elif vegetable == "orange":
        print("I bought orange")
    else:
        print("I bought other vegetables")
        
food("tomato")

def food(vegetable):
    if vegetable == "tomato":
        print("I bought tomato")
    elif vegetable == "orange":
        print("I bought orange")
    else:
        print("I bought other vegetables")
        
food("banana")


def addtwo(a, b):
    added = a + b
    subtracao = a - b
    return added, subtracao

x = addtwo(3,5)
print(x)
print(x[0])
print(x[1])

#Return Values
def greet():
    return "Hello"

print(greet(), "Rodrigo")
print(greet(), "João")

#Multiple Parameters / Arguments
def addtwo(a, b):
    added = a + b
    subtracao = a -b
    return added, subtracao

x = addtwo(3, 5)
print(x)

## Global and local variable

def food():
	bread="white bread"
    
x = food()
print("bread")

def food():
   eggs = "food local" # prints "food local""
   print(eggs)

def more_food():
    eggs = "more_food local"
    print(eggs) # prints "more_food local
    food()
    print(eggs) # prints "more_food local

eggs= "global"
more_food()
print(eggs) # prints "global"


def food():
   eggs = "food local" # prints "food local""
   print(eggs)
   return eggs

def more_food():
    eggs = "more_food local"
    print(eggs) # prints "more_food local
    food_1=food()
    print(eggs) # prints "more_food local
    print(food_1) # prints "more_food local

eggs= "global"
more_food()
print(eggs) # prints "global"

#Assignments operators 

a = 21
b = 10
c = 0

c = a + b
c += a # c + a
c *= a # c * a
c /= a #c / a float
c = 2
c %= a # c % a
c **= a # c** a
c //= a # c/ a int


#Bit operators 
a = 10           # 10 =  1010 
b = 3            # 3 =   0011 

hifen =30
binary = bin(a & b); 
denary = a & b
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a | b); 
denary = (a | b)
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a << 2)
denary = a << 2;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a >> 2); 
denary = a >> 2;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(~a); 
denary =~a;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a ^ b); 
denary =a ^ b;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)