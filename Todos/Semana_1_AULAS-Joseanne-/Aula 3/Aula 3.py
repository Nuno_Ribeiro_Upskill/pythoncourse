# -*- coding: utf-8 -*-
"""
@author: Nuno Ribeiro
"""
################################################################

# Aula 3 PPT 2- PythonConditionalsLoops_part1 

##Comparisons using print https://docs.python.org/3/library/functions.html#ord

print(4==5)
print(6>7)
print(15<100)
print('hello' == 'hello')
print('hello' == 'Hello')
print('dog' != 'cat')
print( True == True)
print(True != False)
print( 42 == 42.0)
print( 42 == "42")
print('apple' == "Apple") 
print("apple" > "Apple") 
print("A unicode is", ord("A") ," ,a unicode is" , ord("a")) #unicode 

##What is the minimum age to drive in Portugal?
##And the maximum age ? 

minimum_age = 18
age = 14
if age > minimum_age:
    print("Congrats, you can get your license")
else :
    print("Please, come back in {0} years" .format(minimum_age - age))  

##Getting Access

name = "Josi"
password = "swordfish"
if name == "Josi":
   print('Hello Josi')
if password == 'swordfish':
   print('Access granted.')
else:
   print('Wrong password.')

##Comparisons using print 
print(4 < 5) and (5 < 6)
print(4 < 5) and (9 < 6)
print(1 == 2) or (2 == 2)
print(2 + 2 == 4 and not 2 + 2 == 5 and 2 * 2 == 2 + 2)

##Code using conditionals 
a = 3
b = 2
if a==5 and b>0: 
      print("a is 5 and",b,"is greater than zero.") 
else: 
      print("a is not 5 or",b,"is not greater than zero.") 


##Repeated Steps

n = 5
while n > 0 :
     print(n)
     n= n - 1
print ("Boom!!")
print(n)

##An Infinite Loop
n = 5
while n > 0 :
    print("Time")
    print("ticking ")
    break
print("Stopped")

##Another Loop
n = 0
while n > 0 :
    print('Time')
    print('ticking ')
print("Stopped")

##Breaking Out of a Loop    
while True:
    line = input('> ')
    if line == 'done' :
        break
    print(line)
print('Done!')

##Finishing an Iteration with continue
while True:
    line = input('> ')
    if line[0] == '#' :
        continue
    if line == 'done' :
        break
    print(line)
print('Done!')

###############################################################
'''Class 2 - 3 - Review
Two new commands to work with the remote repository git pull, git status
To show which data type a variable uses, we use type()
Strings are sequences, we could split sequence using and print it using print(str[_:_]) 
A bit on import sys module and how to use methods available in the module
Exercises on arithmetic operations  
Introduction on how to make decisions using python 
While loop(infinite loop, finite loop)
Basics on fluxograms applied to programming 
Basic analysis on how to make functions (prepare piece of code to be a function)
Update code in files located in the remote repository'''
###############################################################