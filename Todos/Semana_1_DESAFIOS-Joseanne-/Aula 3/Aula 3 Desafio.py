# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 05:35:52 2020

@author: Nuno
"""

##Exercise 1 - Write a code that guess the number stored in a variable   
##Exercise - Change the code

## 1.a.
guess_number = 5
user_number=True
while user_number:
    user_number = int(input("Enter a Number: "))
    if user_number < guess_number:
        print("Please, guess higher")
    elif user_number == guess_number:
      print("Congrats, you found the aswer")
      break
    elif user_number == 9:
        print("No, is not 9")
    else:
        print ("Please, guess smaller.")
        
##Exercise 2 - Write a code to ask  How old are you ? Store the age in a variable and print it 
##Improve your code to check if you have age to retire or not

## 2.a
my_age_ = int(input("Enter your age: "))
print (my_age_)

minimum_age_ = 18
retired_age = 67

## 2.b
if my_age_ <= 18:
    print("You are too young to work, come back to school")
elif (my_age_ > 18 and (my_age_ < 67)) :
    print("Have a good day at Work") 
else :
    print("You have worked enough, Let’s Travel now")
    
##Be careful using conditionals. In this case is not OR but AND.
##Exercise for doing at home  
    
day = "Saturday"
temperature = 21
raining = True


if (day =="Saturday" and temperature > 20 and (not raining)) :
    print("Go out ")
else:
    print ("Better finishing python programming exercises")

##Alternative Code 
day = input("What is the day:")
temperature = int(input("What is the temperature: "))
raining = True
    
if (day =="Saturday" and temperature > 20 and (not raining)) :
    print("Go out ")
else:
    print ("Better finishing python programming exercises") 