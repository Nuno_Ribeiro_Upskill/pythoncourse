# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 05:48:25 2020

@author: Nuno
"""

##Exercise 1  Implement a function that receives a number as parameter and prints, in decreasing order, which numbers are even and which are odd, until it reaches 0.
""">>> even_odd(10)
Even number: 10
Odd number: 9
Even number: 8
Odd number: 7
Even number: 6
Odd number: 5
Even number: 4
Odd number: 3
Even number: 2
Odd number: 1"""

## 1.a.
n=int(input("Enter a number for check odd or even: "))

def printEvenOdd(n):
  while(n>0):
    if (n%2==0):
      print ("Even number:",n)
    else:
      print ("Odd number:",n)
    n=n-1 
    
printEvenOdd(n)

##Exercise 2  Write a function that receives a list of numbers and returns the arithmetic mean of the numbers in  [2, 61, -1, 0, 88, 55, 3, 121, 25, 75]
#The following function must be implemented:

## 2.a.
def mean(numOfList):
    avg = sum(numOfList) / len(numOfList)
    return avg


print("The mean of List is", round(mean([2, 61, -1, 0, 88, 55, 3, 121, 25, 75]), 2))