# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 05:30:27 2020

@author: Nuno
"""
# Aula 2 PPT 2- PythonConditionalsLoops_part1 

# Exercise
##Exercise 1 - Try the following mathematical calculations and guess what is happening: ((3 / 2)), ((3 // 2)), ((3 \% 2)), ((3**2)).
'''Suggestion: check the Python library reference at https://docs.python.org/3/library/stdtypes.html#numeric-types-int-float-complex.'''

## 1.a.
print((3/2))
print((3 // 2))
print((3 % 2 ))
print((3 ** 2 ))

##Exercise 2 - Calculate the average of the following sequences of numbers: (2, 4), (4, 8, 9), (12, 14/6, 15)
# Example to find average of list

## 2.a.
number_list = [2,4]
avg = sum(number_list)/len(number_list)
print("The average is ", round(avg,2))

number_list = [4,8,9]
avg = sum(number_list)/len(number_list)
print("The average is ", round(avg,2))

number_list = [12,14/6,15]
avg = sum(number_list)/len(number_list)
print("The average is ", round(avg,2))

##Exercise 3 - The volume of a sphere is given by (4/3 pi r^3). Calculate the volume of a sphere of radius 5. Suggestion: create a variable named “pi” with the value of 3.1415.
# Python Program to find Volume and Surface Area of Sphere

## 3.a.
PI = 3.1415
radius = float(input('Please Enter the Radius of a Sphere: '))
sa =  4 * PI * radius * radius
Volume = (4 / 3) * PI * radius * radius * radius
 
print("\n The Surface area of a Sphere = %.2f" %sa)
print("\n The Volume of a Sphere = %.2f" %Volume)

##Exercise 4 - Use the modulo operator (%) to check which of the following numbers is even or odd: (1, 5, 20, 60/7).
'''Suggestion: the remainder of (x/2) is always zero when (x) is even.'''

## 4.a.
a = [1,5,20,60/7]
for each in a:
    if each % 2 == 0:
        print(each,' is even')
    else:
        print(each,' is odd')