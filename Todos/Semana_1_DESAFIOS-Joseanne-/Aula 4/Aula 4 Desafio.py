# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 05:44:03 2020

@author: Nuno
"""

## Exercise 1 - Write a code to print out the capital letters in a string: 

'''The Best of made in Portugal – Hats, Soaps, Shoes,Tiles & Ceramics, Cork'''

## 1.a.
# initializing string  
test_str = "The Best of made in Portugal - Hats, Soaps, Shoes, Tites & Ceramics, Cork"
  
# printing original string  
print("The original string is : " + str(test_str)) 
  
# Extract Upper Case Characters  
# Using list comprehension + isupper() 
res = [char for char in test_str if char.isupper()] 
  
# printing result  
print("The uppercase characters in string are : " + str(res)) 

## Exercise 2 - Write a code to print numbers from 0 – 50 divisible by 4. Consider 0 in your code
# Python program to print all the numbers 
# Divisible by 4 for a given number 
  
## 2.a.
# Result function with N 
def result(N): 
      
    # iterate from 0 to N 
    for num in range(N): 
          
            # Short-circuit operator is used  
            if num % 4 == 0 : 
                print(str(num) + " ", end = "") 
                  
            else: 
                pass
# Driver code 
if __name__ == "__main__": 
      
    # input goes here 
    N = 50
      
    # Calling function 
    result(N) 