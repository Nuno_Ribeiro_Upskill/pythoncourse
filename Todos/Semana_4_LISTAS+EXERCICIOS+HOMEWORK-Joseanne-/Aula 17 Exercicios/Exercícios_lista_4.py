# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 02:17:08 2020

@author: Nuno
"""

# Exercises:
# 1-In the Sudoku game we have a 9 x 9 matrix divided into 3 x 3 filled squares previously with some numbers between 1 and 9 (see the example on the left below).
# A solution for an instance of the game is to fill all empty positions with numbers between 1 and 9 respecting the following rules: 
# (a) There cannot be repeated numbers in the same square, that is, each number between 1 and 9 must appear exactly once in each square. 
# (b) There can be no repeated numbers on any line of the matrix. 
# (c) There can be no repeated numbers in any column of the matrix. 
# Write a function that takes a 9 x 9 matrix as a parameter, that represents a proposed solution for a Sudoku, and test if the matrix is a valid solution for the game, returning True if true and False otherwise. The following function must be implemented:
# def isSolution(mat):

"2. For the code in Figure 2:"
"(a) Determine the local and global variables for this program. For each variable identify which function it belongs to."
"(b) Show what will be printed on the computer screen when this program is run."

j = 1
def main ():
    a = 9
    if a % 2 ==0:
       a = 2
    else :
        a = 3
        
    print ( fun1 (2 , 4))
    
    for i in range (3) :
        for j in range (3) :
            print ( fun1 (a, i+j))

def fun1 (a, b):
    p = 1
    for i in range (b):
        p = p * a
    return p+j

main ()

# a)
# j- global varible
# a is loclal variable in main
# i is local varible - in both fuctions have i variable but the i's in two functions are different
# p is local varible in fun1()
# b)
# Output
# 17
# 2
# 4
# 10
# 4
# 10
# 28
# 10
# 28
# 82
        

"3. Write a program that reads a sequence of whole numbers and saves them in a list. Then, the program must read another integer A and the program must find two different position numbers in the list whose multiplication is C and print them out. If there are no such numbers, the program must print a corresponding message."
# List = [1 2 3 7 11 13]
# A = 77
# Output: 7 and 11
# A = 50 
# Output:  values not found  

# creating an empty list 
list = [] 

print("Input the elements:")
#infinite lop
while(1):
	#try and get input until double enter
	try:
		ele = int(input()) 
		list.append(ele) # adding the element 
	#if no input break
	except:
		break

#getting value
while(1):
	count=0
	try:
		val=int(input("Give the integer : "))

		#going through every element
		for i in range(len(list)):
			for j in range(i+1,len(list)):
				if(list[i]*list[j]==val):
					count=count+1
					print(str(list[i])+" and "+str(list[j]))
		if(count==0):
			print("Not found")
	except:
		break