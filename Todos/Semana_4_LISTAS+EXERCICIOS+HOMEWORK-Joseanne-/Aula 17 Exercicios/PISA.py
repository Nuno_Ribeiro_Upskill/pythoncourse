# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 05:10:21 2020

@author: Nuno
"""
"No anexo um arquivo csv com os dados do PISA de matemática de Portugal (avaliação global da educação)"
"Gostaria de ver um gráfico de médias obtidas por ano de portugal (PRT),"
"um outro de 5 outros os países(livre escolha) comparados com Portugal"
"e um terceiro gráfico de barras com os dados em relação ao desempenho de meninos e as meninas usando o matplotlib   https://data.oecd.org/pisa/mathematics-performance-pisa.htm#indicator-chart"


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker

df= pd.read_csv("C:/Users/admin/Desktop/DP_LIVE_04112020161547500.csv")
df.head(5)

d1=df[df['LOCATION'] == 'PRT']
d2 = d1.drop_duplicates(subset='TIME')

d2.plot(x= 'TIME', y= 'Value',figsize=(10,5),label= 'Portugal')
xticks = [2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018]
ticklabels = ['2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018']
plt.xticks(xticks, ticklabels)

plt.gca().xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

plt.show()



d3=df[df['LOCATION'] == 'OAVG']
d3 = d3.drop_duplicates(subset='TIME')


h2=df[df['LOCATION'] == 'CAN']
h2 = h2.drop_duplicates(subset='TIME')

h3=df[df['LOCATION'] == 'CHE']
h3 = h3.drop_duplicates(subset='TIME')

h4=df[df['LOCATION'] == 'JPN']
h4 = h4.drop_duplicates(subset='TIME')

h5=df[df['LOCATION'] == 'AUS']
h5 = h5.drop_duplicates(subset='TIME')

h6=df[df['LOCATION'] == 'ESP']
h6 = h6.drop_duplicates(subset='TIME')

x1=d2['TIME']
y1=d2['Value']
x2=d3['TIME']
y2=d3['Value']
x3=h2['TIME']
y3=h2['Value']
x4=h3['TIME']
y4=h3['Value']
x5=h4['TIME']
y5=h4['Value']
x6=h5['TIME']
y6=h5['Value']
x7=h6['TIME']
y7=h6['Value']

plt.plot(x1,y1, label= 'Portugal')
plt.plot(x2,y2, label = 'OECD average')
plt.plot(x3,y3, label = 'Canada')
plt.plot(x4,y4, label = 'Switzerland')
plt.plot(x5,y5, label = 'Japan')
plt.plot(x6,y6, label = 'Australia')
plt.plot(x7,y7, label = 'Spain')
xticks = [2003,2018]
ticklabels = ['2003','2018']
plt.xticks(xticks, ticklabels)
plt.gca().xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
plt.legend(bbox_to_anchor=(1, 1),loc='upper left')
plt.show()