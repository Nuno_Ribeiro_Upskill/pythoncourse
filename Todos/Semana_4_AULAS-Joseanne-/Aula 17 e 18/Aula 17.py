# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 03:28:06 2020

@author: Nuno
"""
##NumPy and Matplotlib introduction

import numpy as np
Students = np.array([[[1, 3, 5], [1, 1, 1]],
                     [[4.5, 4, 5], [4.3,4.4,4.6]]])

import numpy as np
print(np.__version__)         
print(np.version)     
              
print(dir(np)) 
print(dir(np.array))

##Arrays
students = np.array([[[1, 3, 5], [1, 1, 1]],
                     [[4.5, 4, 5], [4.3,4.4,4.6]]]) 
print( students.ndim, students.dtype)

##Arithmetic operators: elementwise application
xl = np.array([[-1, 3]])
x2 = np.array([[1, 2]]) 
x3 = xl+x2 
x4 = xl*x2
x5 = xl-x2
print(xl) 
print(x2)
print(x3)
print(x4)
print(x5)

##Data types  
x3 = np.power(10, 4, dtype=np.int8) 
print(x3) #16 overflow int8 size 
x4 = np.power(10, 4, dtype=np.int32) 
print(x4) # 10000 correct value

##Reshape Vs Resize
print(np.arange(10))
print(np.arange(10).reshape(2,5))

print(np.arange(10).reshape(2,7))
# Value Error: cannot reshape array of size 10 into shape (2,7)

print((np.resize(np.arange(10), (2, 5))))

print((np.resize(np.arange(10),(2,7))))


##Newaxis :  adds a colum in the array  
##Copy : copy the array to other array 

d = np.arange(2, 5)
print(np.arange(2,5))
print(d.shape)
print(d[: np.newaxis])
print(d[: np.newaxis].shape)

x = np.array([1, 4, 3])

y = x
z = np.copy(x) 
x[1] = 2 
print(x) 
print(y) 
print(z)

##Sorting  : organizing a array using a specific parameter
dtype = [('name', 'S10'), ('grade', float), ('age', int)]
values = [('Joseanne', 5, 31),	('Hamed',5, 32),
('Stefan', 5, 24)]	
sorted_data = np.array(values, dtype=dtype)
print(np.sort(sorted_data, order= 'age'))

#1Creating Zeros vectors and Matrices           
print(np.zeros((3))) 
print(np.zeros((3,4)))

b = np.zeros((10, 6),dtype=int) 
print(b)
print(b.ndim, b.dtype, b.size)

b = np.zeros(10)
b[8] = 1
print(b)

##Slicing
b = np.arrange(10,50)
print(b)
print(b.ndim, b.dtype, b.size)


b = np.arange(50)
b = b[::-1]
b = b.reshape(5,10)
print(b)
print(b[4,1])
print(b[3,:])
print(b[0,2:])
print(b[:1,1:])

##Concatenation, vstack, hstack 
b = np.arange(50)
b = b.reshape(5,10)
c = np.ones((2,10)) 
print(np.concatenate((b,c)))

a = np.arange(2,6)
b = np.array([[3,5], [4,6]]) 
print(np.vstack(a)) 
print(np.hstack(b))

##Ones,, zeros_like, ones_like, eye, empty, full 
d = np.ones((1, 3))
x = np.zeros_like((d)) 
x1 = np.ones_like((x)) 
print(d) 
print(x)
print(x1)

b = np.eye(3)
print(b)
print(np.empty((1,2))) 
print(np.full((2,2),np.inf))

##Astype
b = np.arange(50) 
c = b.astype(np.int16) 
print(b.astype(np.int16)) 
print( b.ndim, b.dtype, b.size) 
print( c.ndim, c.dtype, c.size)

#Random Numbers 
b = np.random.random((10,10)) 
bmin, bmax = b.min(), b.max() 
print(bmin, bmax)

b = np.random.random((3,3,3)) 
print(b)

##Usefull funcionality 
yesterday = np.datetime64('today') - np.timedelta64(1)
today = np.datetime64('today')
tomorrow = np.datetime64('today') + np.timedelta64(1) 
print(yesterday, today, tomorrow)
b = np.arange('2019 ', '2021', dtype= 'datetime64[D]')
print(b.size, b.ndim)

print (np.ones ( (2, 6) )+np. ones((2,6)))

xl = np.array([[-1, 3],[4,2]]) 
x2 = np.array([[1, 2],[3,4]]) 
x4 = xl+x2
x5 = xl*x2
x3 = np.dot(xl,x2)
print(x3)
print(x4)
print(x5)


import matplotlib.pyplot as plt

values = np.linspace(-(2*np.pi),2*np.pi, 20)
cos_values = np.cos(values)
sin_values = np.sin(values)

plt.plot(cos_values, color = 'bLue', marker= '*')
plt.plot(sin_values, color = 'red')
plt.show()

##Imaging Processing 
from PIL import Image

my_path = "C:\\Users\\Nuno\\Desktop"
#	im = Image.open(my_path)
#	plt.imshow(im)


# im2 = Image.open(my_path) 
# im_p2 = np.array(im2.convert('P')) 
# plt.imshow(im_p2)

im3 = Image.open(my_path) 
im_p3 = np.array(im3.convert('L')) 
plt.imshow(im_p3)