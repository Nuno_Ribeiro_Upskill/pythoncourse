# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 03:07:07 2020

@author: Nuno
"""
## Importing modules
##List

import sqlite3
from sqlite3 import Error 
from numpy import * 
import numpy as np

b = [4,6,7] 
print(np.add(b, 5))

print (dir(np))

L = [3, True, "ali", 2.7, [5,8]]

a = [3, [109, 27], 4, 15] 
# print(a(1))
# Type Error: "list" is not callable
print (a[1])

a = [ 7, 5, 30, 2, 6, 25] 
print(a[1:4])

a = []
for i in range (4):
    a.append(i)
print(a)

a = [1, 3, 6, 5, 3]
print(a.count(3))

## Tuple
t = ("EngLish", "History", "Mathematics")
print(t[1])
print(t.index("EngLish"))


t = (1, 9, 3, 9)
print(t.count(9))
print(max(t))
print(t + t)


print((1,2) == (2,1))

t = (4, 7, 2, 9, 8)
x = list (t)
x.remove(2)
t = tuple(x)
print(t)

a = (1,2)
b = (3,4)
c = zip(a,b)
x = list(c) 
print(x)

print(x[0] )

z = ((1,3), (2,4))
u = zip(*z)
print(list(u))

## Dictionaries

d = {"brand": "cherry", "model": "arizo5","color": "white"}
d ["color"] = "black"
print(d)
                       
x = d.get("model")
print(x)

print(list(d.keys())) 

print(list(d.values())) 

d.pop("model")
print(d)
                     
num = { "ali" : [12, 13, 8], "sara": [15, 7, 14], "taha": [5,18,13]}
d = {k : sorted(v) for k, v in num.items()}
print(d)

k = ["red", "green"]
v = ["#FF0000", "#008000"]
Z = zip (k,v)
d = dict(z)
print(d)

##Set
f = {"apple", "orange", "banana"}
f.add("cherry")
print(f)

f.update(["mango", "grapes"])
print(f)

f.remove(["apple"])
print(f)

x = {1, 2, 3}
y = {2, 3, 4}
print(X.union(Y))
print(X | Y)

print(X.intersection(Y))
print(X & Y)


A = {1, 2, 4}
B = {1, 2, 3, 4, 5}
print(A.issubset(B))

print(B.issubset(A))

##Open Files
encoding_type = "utf-8"
epopeia = open ("C:\\Users\\admin\\Desktop\\pythoncode\\epopeia.txt", "r", encoding=encoding_type)
                
for line in epopeia: 
    print(line) 
    if "tempestade" in line.lower(): 
        print (line)

epopeia.close()

encoding_type = "utf-8"
file_path = "C:\\Users\\admin\\Desktop\\pythoncode\\epopeia.txt"
with open(file_path, "r", encoding=encoding_type) as epopeia:
    for line in epopeia:
        if "tempestade" in line.lower(): 
            print (line)
print (line)                    
                      
file_path = "C:\\Users\\admin\\Desktop\\pythoncode\\epopeia.txt"
with open(file_path, "r", encoding=encoding_type) as epopeia:
    line = epopeia.readline()
while line:
    print(line, end='')	
    line = epopeia.readline()          
     
file_path = "C:\\Users\\admin\\Desktop\\pythoncode\\epopeia.txt"                
with open(file_path, "r", encoding=encoding_type) as epopeia:
    poem = epopeia.readlines() 
    print(type(poem))
print(poem)

file_path = "C:\\L/sers\\admin\\Desktop\\pythoncode\\epopeia.txt"
with open(file_path, 'r', encoding=encoding_type) as epopeia:
    text = epopeia.read() 
    print(type(text)) 
print(text)

for linea in poem[::-1] :
    print(linea, end='') 
print(type(poem))
                           
for linea in text[::-l] : 
    print(linea, end='') 
print(type(text))

# {
# "data": [
# {
# 	"id": "12341 ",
# 	"name ": "Joseanne Viana",
# 	"address": {
# 		"city": "Lisboa",
# 		"state ": "Lisboa ",
# 		"postal_code":"1000569"
# 	           },
# 	"position ": "PhD Student "
# 		}

#         ]
# }

##Json Files
import json
file_path = "C:\\Users\\admin\\Desktop\\pythoncode5\\data.json" 
with open(file_path) as json_file:
    json_object = json.load(json_file) 
    name = json_object['data'][0]['name'] 
    address= json_object['data'][1]['address']['city']
print(json_object) 
print(name)
print(adress)

##Excel Files
import pandas as pd
a = "C:\\Users\\admin\\pythoncode\\Employee_data.xLsx" 
all_data = pd.read_excel(a, index_col=1)

email = all_data["email"].head()
company_name = all_data['company_name']
xl = pd.ExcelFile(a) 
df = xl.parse("b")
                 
#Try, else, finally 
                
import json
file_path = 'C:\\Users\\admin\\Desktop\\pythoncode5\\data.json'
try :
    with open(file_path) as json_file: #path is not correct 
        json_object = json.load(json_file)
except :
    print("File not found")
    
else :
    name = json_object['data'][0]['name'] 
    address= json_object['data'][1]['address']['city']

    print(json_object) 
    print(name) 
    print(address)

finally :
    print("Something happened")

##Lambda expressions
def power_tree(x):
    return(x**3)
print(power_tree(10))

a = lambda x : x**3
print(a(10))

def larger_num(numl, num2):
    if num1 > num2:
        return num1
    else:
        return num2
print(larger_num(5, 7))

larger_num = lambda num1, num2: num1 if num1 > num2 else num2 
print(larger_num(5, 7))

##Maps
text = "LISBON IS IN PORTUGAL "
def char_lowercase():
    char_lowercase = [char.lower() for char in text] 
    return char_lowercase

#	use map
def map_lowercase():
    map_lowercase = list(map(str.lower, text)) 
    return map_lowercase

def comp_words(): 
    words_lowercase = [word.lower() for word in text.split(' ')] 
    return words_lowercase

#	use map
def map_words():
    map_w = list(map(str.lower, text.split(' '))) 
    return map_w

print(char_lowercase())
print(map_lowercase()) 
print(comp_words()) 
print(map_words())

                                 
##Filter
vegetables = [
    ["Beets", "CauLif Lower", "BroccoLi"], 
    ["Beets", "Carrot", "CauLiflower"], 
    ["Beets", "Carrot"],
    ["Beets", "BroccoLi", "Carrot"],
]

def not_brocoli(food_list: list): 
    return "BroccoLi" not in food_list

brocoli_less_list = list(filter(not_brocoli, vegetables))

print(brocoli_less_list)

##Classes
class josi:
    a = [2, 4, 6 ]
    b = [7,8] 
    c =[10,11,21]   
    def __init__(self, val):
        self.val = val 
        print(val)
    def increase(self): 
        self.val += 1 
    def show(self): 
        print(self. val)
        
x = josi(3) 
c = x.increase() 
d = x.show() 
print(type(x.a)) 
print(x.a.__add__(x.b))
print(x.a.__eq__(x.b))
print(x.a.__ne__(x.c))